Humblesetuptools is a fork of setuptools that doesn't install to .egg
directories, and doesn't mess with your sys.path. pip already works around this,
humblesetuptools just removes the option to do it.

*Use this at your own risk.*

For information about setuptools, see `setuptools on PyPI
<https://pypi.python.org/pypi/setuptools>`_.
